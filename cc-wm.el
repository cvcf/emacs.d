(require 'cc-utils)

(defun cc/launch-app ()
  "Prompt for an application and launch it."
  (interactive)
  (call-interactively 'async-shell-command))

(defun cc/assign-monitors-to-workspaces ()
  "Assign the active monitors to EXWM workspaces."
  ;; TODO: Figure out how to do this dynamically
  '(0 "DP-1-1" 1 "DP-1-1" 2 "DP-1-2" 3 "DP-1-2"))

(require 'windower)

(require 'desktop-environment)
(cc/enable desktop-environment-mode)

;; https://github.com/ch11ng/exwm
;; - See also: https://gitlab.com/ambrevar/dotfiles/-/blob/master/.emacs.d/lisp/init-exwm.el
;; - See also: https://github.com/johanwiden/exwm-setup
;; - See also: https://wiki.archlinux.org/title/EXWM
(require 'exwm-systemtray)
(require 'exwm-manage)
(require 'exwm-randr)
(require 'browse-url)
(require 'windower)

(setq
 browse-url-generic-program "nyxt"
 browse-url-browser-function 'browse-url-generic

 exwm-systemtray-height 16

 mouse-autoselect-window nil
 focus-follows-mouse t
 exwm-workspace-warp-cursor t

 exwm-randr-workspace-monitor-plist (cc/assign-monitors-to-workspaces)

 window-divider-default-bottom-width 2
 window-divider-default-right-width 2)

(cc/enable window-divider-mode)

;; TODO: Figure out why `unbind-key' is apparently undefined
;; (unbind-key (kbd "s-l") 'desktop-environment-mode-map)
(exwm-input-set-key (kbd "s-l") nil)

(exwm-input-set-key (kbd "s-SPC") #'cc/launch-app)
(exwm-input-set-key (kbd "s-q") #'desktop-environment-lock-screen)
(exwm-input-set-key (kbd "s-x") #'exwm-input-toggle-keyboard)
(exwm-input-set-key (kbd "s-R") #'exwm-reset)
(exwm-input-set-key (kbd "s-D") #'kill-this-buffer)

(exwm-input-set-key (kbd "s-<tab>") #'windower-switch-to-last-buffer)
(exwm-input-set-key (kbd "s-1") #'windower-toggle-single)
(exwm-input-set-key (kbd "s--") #'windower-toggle-split)
(exwm-input-set-key (kbd "s-H") #'windower-swap-left)
(exwm-input-set-key (kbd "s-J") #'windower-swap-down)
(exwm-input-set-key (kbd "s-K") #'windower-swap-up)
(exwm-input-set-key (kbd "s-L") #'windower-swap-right)

(exwm-input-set-key (kbd "s-h") #'windmove-left)
(exwm-input-set-key (kbd "s-j") #'windmove-down)
(exwm-input-set-key (kbd "s-k") #'windmove-up)
(exwm-input-set-key (kbd "s-l") #'windmove-right)

(exwm-input-set-key (kbd "s-w") #'exwm-workspace-switch)

(require 'exwm)

(exwm-systemtray-enable)
(exwm-randr-enable)
(exwm-enable)
(exwm-init)

(provide 'cc-wm)
