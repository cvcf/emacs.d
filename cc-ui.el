(require 'cc-utils)

(defun cc/set-font (face &rest args)
  "Configure the specified font `face'.

The arguments passed to this function should begin with the name of
the font-family and the rest should be attributes that can be passed
to `set-face-attribute'."
  (apply #'set-face-attribute face nil :family args))

(cc/set-font 'default "Fira Code Retina" :height 110)

(require 'all-the-icons)

(let ((font-dir (concat (getenv "HOME") "/.local/share/fonts/")))
  (unless (file-exists-p (concat font-dir "all-the-icons.ttf"))
    (all-the-icons-install-fonts t)))

(require 'doom-modeline)

(cc/enable doom-modeline-mode)

(setq
 doom-themes-enable-bold t
 doom-themes-enable-italic t)

(load-theme 'doom-dracula t)

;; TODO: Add when we install Neotree
;;(doom-themes-neotree-config)
(doom-themes-visual-bell-config)
(doom-themes-org-config)

(provide 'cc-ui)
