(require 'cc-vars)

(defun cc/configure-modes (modes action)
  "Call the mode function in `modes' passing `action'."
  (mapcar (lambda (_) (funcall _ action)) modes))

(defmacro cc/enable (&rest modes)
  "Enable all the provided `modes'."
  `(progn ',(cc/configure-modes modes 1)))

(defun cc/enable-mode-for (hook mode)
  "Enable `mode' when `hook' runs it's functions."
  (add-hook hook `(lambda () (cc/enable ,mode))))

(defmacro cc/disable (&rest modes)
  "Disable all the provided `modes'."
  `(progn ',(cc/configure-modes modes -1)))

(defun cc/disable-mode-for (hook mode)
  "Disable `mode' when `hook' runs it's functions."
  (add-hook hook `(lambda () (cc/disable ,mode))))

(defun cc/ensure-directory-exists (directory)
  "Create `directory' if it doesn't already exist."
  (unless (file-exists-p directory)
    (mkdir directory t)))

(defun cc/maybe-load-file (file)
  "Load the specified `file', if it exists."
  (when (file-exists-p file)
    (load file)))

(defun cc/get-shell-path (shell)
  "Return the absolute path to `shell'; or `nil' if it's not found."
  (let ((result (string-trim (shell-command-to-string (format "which %s" shell)))))
    (unless (string-match (format "^which: no %s" shell) result)
      result)))

(defun cc/start-shell (&optional shell)
  "Start a shell.

If provided, start `shell'; otherwise, start the shell in `cc/default-shell-name'."
  (interactive)
  (ansi-term (cc/get-shell-path (or shell cc/default-shell-name))))

(defun cc/delete-this-buffer ()
  "Delete the current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defun cc/switch-to-messages ()
  "Switch to the messages buffer."
  (interactive)
  (switch-to-buffer (messages-buffer)))

(defun cc/switch-to-scratch ()
  "Switch to the scratch buffer."
  (interactive)
  (switch-to-buffer "*scratch*"))

(defun cc/delete-this-file ()
  "Delete the file associated with the current buffer."
  (interactive)
  (let ((n (buffer-file-name)))
    (cond (n (delete-file n)
             (cc/delete-this-buffer))
          (t (message "'%s' is not associated with a file." (buffer-name))))))

(provide 'cc-utils)
