(require 'subr-x)
(require 'cc-utils)
(require 'cc-vars)

(require 'cc-evil)

;; Using evil keybinding macros
(evil-define-key normalish-modes 'global normalish-prefix '("root" keymap))
(evil-define-key insertish-modes 'global insertish-prefix '("root" keymap))
(evil-define-key normalish-modes 'global
  (concat normalish-prefix "'") '("start shell" . cc/start-shell)
  (concat normalish-prefix " ") '("exec cmd" . execute-extended-command)

  (concat normalish-prefix "b")  '("buffers" keymap)
  (concat normalish-prefix "bb") '("switch" . switch-to-buffer)
  (concat normalish-prefix "bd") '("delete" . cc/delete-this-buffer)
  (concat normalish-prefix "bm") '("messages buffer" . cc/switch-to-messages)
  (concat normalish-prefix "bs") '("scratch buffer" . cc/switch-to-scratch)
  (concat normalish-prefix "bn") '("next" . next-buffer)
  (concat normalish-prefix "bp") '("previous" . previous-buffer)

  (concat normalish-prefix "f")  '("files" keymap)
  (concat normalish-prefix "fd") '("delete" . cc/delete-this-file)
  (concat normalish-prefix "ff") '("find" . find-file)
  (concat normalish-prefix "fs") '("save" . save-buffer)
  (concat normalish-prefix "fj") '("jump to dir" . dired-jump)
  (concat normalish-prefix "fr") '("find recent" . counsel-recentf)

  (concat normalish-prefix "i") '("insert" keymap)

  (concat normalish-prefix "k")  '("lisp" keymap)
  (concat normalish-prefix "kw") '("wrap" . sp-wrap-round)
  (concat normalish-prefix "kW") '("unwrap" . sp-unwrap-sexp)

  (concat normalish-prefix "n")  '("notes" keymap)
  (concat normalish-prefix "na") '("agenda" . org-agenda)
  (concat normalish-prefix "nc") '("capture" . org-capture)
  (concat normalish-prefix "ni") '("insert link" . org-insert-link)
  (concat normalish-prefix "nl") '("store link" . org-store-link)

  (concat normalish-prefix "m")  '("major mode" keymap)

  (concat normalish-prefix "q")  '("quit" keymap)
  (concat normalish-prefix "qq") '("quit emacs" . save-buffers-kill-terminal)

  (concat normalish-prefix "w")  '("windows" keymap)
  (concat normalish-prefix "wd") '("delete" . evil-window-delete)
  (concat normalish-prefix "wh") '("left" . evil-window-left)
  (concat normalish-prefix "wj") '("down" . evil-window-down)
  (concat normalish-prefix "wk") '("up" . evil-window-up)
  (concat normalish-prefix "wl") '("right" . evil-window-right)
  (concat normalish-prefix "wo") '("other window" . other-window)
  (concat normalish-prefix "w-") '("split horizontally" . evil-window-split)
  (concat normalish-prefix "ws") '("split horizontally" . evil-window-split)
  (concat normalish-prefix "wv") '("split vertically" . evil-window-vsplit))

(provide 'cc-keys)
