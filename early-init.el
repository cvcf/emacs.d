;;; -*- lexical-binding: t; -*-

(push (expand-file-name "lisp" user-emacs-directory) load-path)
(push (expand-file-name "lisp/packages" user-emacs-directory) load-path)

(setq package-enable-at-startup nil)

(setq gc-cons-threshold (* 50 1000 1000))
