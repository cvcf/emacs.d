(defvar cc/guix? (and (string= "gnu/linux" system-type) (file-exists-p "/gnu/store"))
  "Return `t' if the current system is a Guix System; `nil', otherwise.")

(defvar cc/default-shell-name "fish"
  "The name of the default shell to use when starting a new one.")

(defvar normalish-prefix (kbd "SPC")
  "The prefix key to use for normalish modes. See `normalish-modes'")

(defvar normalish-modes '(normal motion visual)
  "A list of modes that are similar enough to normal mode that they should use the `normalish-prefix'.")

(defvar insertish-prefix (kbd "C-SPC")
  "The prefix key to use for insertish modes. See `insertish-modes'")

(defvar insertish-modes '(emacs insert replace)
  "A list of modes that are similar enough to insert mode that they should use the `insertish-prefix'.")

(provide 'cc-vars)
