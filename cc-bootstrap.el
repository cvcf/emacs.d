(require 'cc-vars)

(defvar bootstrap-version)

(defun cc/bootstrap-straight ()
  "Bootstrap the straight.el package manager."
  (let ((bootstrap-url "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el")
        (bootstrap-file (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer (url-retrieve-synchronously bootstrap-url 'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(defun cc/configure-straight-with-defaults ()
  "Set initial default configuration for the straight.el package manager."
  (setq straight-use-package-by-default t)
  (setq straight-use-package-version 'straight))

(unless cc/guix?
  (cc/bootstrap-straight)
  (cc/configure-straight-with-defaults)

  ;; https://github.com/jwiegley/use-package
  (straight-use-package 'use-package))

(provide 'cc-bootstrap)
