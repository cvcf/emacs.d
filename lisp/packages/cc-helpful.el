(require 'cc-vars)

(require 'helpful)

(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes 'global
    (concat normalish-prefix "h.") '("describe at point" . helpful-at-point)
    (concat normalish-prefix "hf") '("describe callable" . helpful-callable)
    (concat normalish-prefix "hi") '("info" . info)
    (concat normalish-prefix "hk") '("describe key" . helpful-key)
    (concat normalish-prefix "hm") '("describe mode" . describe-mode)
    (concat normalish-prefix "ho") '("describe symbol" . helpful-symbol)
    (concat normalish-prefix "hs") '("search help" . apropos)
    (concat normalish-prefix "hv") '("describe variable" . helpful-variable)))

(provide 'cc-helpful)
