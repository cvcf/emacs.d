(require 'cc-vars)

(setq
 ac-auto-start 1
 ac-delay 0.2)

(require 'auto-complete)

;(ac-config-default)
(cc/enable global-auto-complete-mode)

(provide 'cc-auto-complete)
