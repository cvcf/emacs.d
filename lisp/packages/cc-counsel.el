(require 'cc-vars)

(setq
 ivy-count-format "(%d/%d) "
 ivy-use-virtual-buffers t
 enable-recursive-minibuffers t)

(require 'counsel)

(with-eval-after-load 'cc-keys
  (evil-define-key '(normal motion visual) 'global
    (kbd "C-s") '("search" . swiper))

  (define-key ivy-minibuffer-map (kbd "C-,") #'ivy-beginning-of-buffer)
  (define-key ivy-minibuffer-map (kbd "C-.") #'ivy-end-of-buffer)
  (define-key ivy-minibuffer-map (kbd "C-j") #'ivy-next-line)
  (define-key ivy-minibuffer-map (kbd "C-k") #'ivy-previous-line)
  (define-key ivy-minibuffer-map (kbd "C-l") #'ivy-alt-done)
  (define-key ivy-minibuffer-map (kbd "TAB") #'ivy-alt-done)

  (define-key ivy-switch-buffer-map (kbd "C-j") #'ivy-next-line)
  (define-key ivy-switch-buffer-map (kbd "C-k") #'ivy-previous-line)
  (define-key ivy-switch-buffer-map (kbd "C-l") #'ivy-done)
  (define-key ivy-switch-buffer-map (kbd "C-d") #'ivy-switch-buffer-kill)

  (define-key ivy-reverse-i-search-map (kbd "C-j") #'ivy-next-line)
  (define-key ivy-reverse-i-search-map (kbd "C-k") #'ivy-previous-line)
  (define-key ivy-reverse-i-search-map (kbd "C-d") #'ivy-switch-buffer-kill))

(cc/enable ivy-mode counsel-mode)

(provide 'cc-counsel)
