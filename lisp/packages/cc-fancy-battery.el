(require 'cc-utils)

(setq fancy-battery-show-percentage t)

(require 'fancy-battery)

(cc/enable fancy-battery-mode)

(provide 'cc-fancy-battery)
