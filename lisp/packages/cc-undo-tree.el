(setq undo-tree-auto-save-history nil)

(require 'undo-tree)

(cc/enable undo-tree-mode)

(provide 'cc-undo-tree)
