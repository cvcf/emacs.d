(require 'cc-vars)

(setq password-generator-paranoid-length 25)

(require 'password-generator)

(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes 'global
    (concat normalish-prefix "i") '("insert" keymap)
    (concat normalish-prefix "ip") '("password" keymap)
    (concat normalish-prefix "ip1") '("simple" . password-generator-simple)
    (concat normalish-prefix "ip2") '("strong" . password-generator-strong)
    (concat normalish-prefix "ip3") '("paranoid" . password-generator-paranoid)
    (concat normalish-prefix "ipn") '("numeric" . password-generator-numeric)
    (concat normalish-prefix "ipp") '("phonetic" . password-generator-phonetic)))

(provide 'cc-password-generator)
