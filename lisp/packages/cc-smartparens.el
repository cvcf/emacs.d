(require 'cc-vars)

(require 'smartparens)

(cc/enable smartparens-global-mode)

(provide 'cc-smartparens)
