(require 'cc-vars)

(setq
  projectile-indexing-method 'hybrid
  projectile-sort-order 'recently-active
  projectile-enable-caching t)

(require 'projectile)

(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes 'global
    (concat normalish-prefix "p.") '("commander" . projectile-commander)
    (concat normalish-prefix "p'") '("pop shell" . projectile-run-shell)
    (concat normalish-prefix "pc") '("compile" . projectile-compile-project)
    (concat normalish-prefix "pd") '("directory" . projectile-find-dir)
    (concat normalish-prefix "pf") '("file" . projectile-find-file-dwim)
    (concat normalish-prefix "pk") '("kill buffers" . projectile-kill-buffers)
    (concat normalish-prefix "pp") '("switch project" . counsel-projectile-switch-project)
    (concat normalish-prefix "pt") '("test" . projectile-test-project)
    (concat normalish-prefix "pF") '("file in all projects" . projectile-find-file-in-known-projects)))

(cc/enable projectile-mode)

(provide 'cc-projectile)
