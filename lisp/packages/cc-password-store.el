(require 'cc-vars)

(require 'pass)
(require 'password-store)
(require 'password-store-otp)

(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes 'global
    (concat normalish-prefix "a")    '("apps" keymap)
    (concat normalish-prefix "ap")   '("pass" keymap)
    (concat normalish-prefix "apf")  '("copy field" . password-store-copy-field)
    (concat normalish-prefix "api")  '("insert" . password-store-insert)
    (concat normalish-prefix "ape")  '("edit" . password-store-edit)
    (concat normalish-prefix "apy")  '("copy password" . password-store-copy)
    (concat normalish-prefix "apg")  '("generate" . password-store-generate)
    (concat normalish-prefix "apr")  '("remove" . password-store-remove)
    (concat normalish-prefix "apC")  '("clear" . password-store-clear)
    (concat normalish-prefix "apR")  '("rename" . password-store-rename)

    (concat normalish-prefix "apo")  '("otp" keymap)
    (concat normalish-prefix "apoa") '("append" . password-store-otp-append)
    (concat normalish-prefix "apoA") '("append from image" . password-store-otp-append-from-image)
    (concat normalish-prefix "apoi") '("insert" . password-store-otp-insert)
    (concat normalish-prefix "apoy") '("copy token" . password-store-otp-token-copy)
    (concat normalish-prefix "apou") '("copy uri" . password-store-otp-uri-copy)))

(provide 'cc-password-store)
