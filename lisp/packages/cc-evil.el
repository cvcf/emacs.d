(require 'cc-utils)
(require 'cc-vars)

(setq
 evil-want-integration t
 evil-want-keybinding nil
 evil-move-beyond-eol t)

(setq
 undo-tree-auto-save-history nil)

(require 'evil)
(cc/enable evil-mode)

(cond ((= 28 emacs-major-version)
       (setq evil-undo-system 'undo-redo)
       (cc/disable global-undo-tree-mode))
      (t
       (setq evil-undo-system 'undo-tree)
       (require 'cc-undo-tree)))

(require 'evil-collection)
(evil-collection-init)

(require 'evil-commentary)
(cc/enable evil-commentary-mode)
(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes 'global
    (concat normalish-prefix ";") '("un/comment" . evil-commentary)))

(require 'evil-surround)
(cc/enable global-evil-surround-mode)

(provide 'cc-evil)
