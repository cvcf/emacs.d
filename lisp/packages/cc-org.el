(require 'cc-vars)

(setq
 org-directory (expand-file-name "~/org")
 org-archive-directory (expand-file-name "archive" org-directory)
 org-hide-leading-stars t
 org-return-follows-link t
 org-enforce-todo-dependencies t
 org-enforce-todo-checkbox-dependencies t
 org-startup-folded 'folded
 org-catch-invisible-edits 'show
 org-todo-keywords
 '((sequence "TODO(t)" "STARTED(s!)" "WAITING(w@/!)" "BLOCKED(b@/!)" "|"
             "DONE(d!)" "DELEGATED(D!)")
   (type "INCOMPLETE(I!)" "SKIPPED(S@/!)" "POSTPONED(P@/!)" "CANCELLED(C@/!)" "NOTE(n)"))
 org-todo-keyword-faces
 '(("TODO"       . (:foreground "#ffdfba" :weight "bold"))
   ("DONE"       . (:foreground "#d4ffea" :weight "bold"))
   ("NOTE"       . (:foreground "#fbbb62" :weight "bold"))
   ("STARTED"    . (:foreground "#fff27c" :weight "bold"))
   ("WAITING"    . (:foreground "#ffb3ba" :weight "bold"))
   ("BLOCKED"    . (:foreground "#ff3d3d" :weight "bold"))
   ("DELEGATED"  . (:foreground "#ccccff" :weight "bold"))
   ("INCOMPLETE" . (:foreground "#ffd1dc" :weight "bold"))
   ("SKIPPED"    . (:foreground "#70719d" :weight "bold"))
   ("POSTPONED"  . (:foreground "#ab9de3" :weight "bold"))
   ("CANCELLED"  . (:foreground "#bae1ff" :weight "bold")))
 org-log-into-drawer t
 org-log-done t
 org-hide-emphasis-markers t
 org-tags-column -99
 org-complete-tags-always-offer-all-agenda-tags t
 org-tag-persistent-alist
 '((:startgrouptag . nil) ("projects" . ?r)
   (:grouptags . nil)
   ("idea" . ?i) ("software" . ?f) ("hardware" . ?d) ("electronics" . ?c) ("diy" . ?y)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("work" . ?w)
   (:grouptags . nil)
   ("job" . ?j) ("business" . ?b)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("job" . ?j)
   (:grouptags . nil)
   ("zaden" . ?z) ("ngc" . ?g)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("zaden" . ?z)
   (:grouptags . nil)
   ("admin" . ?t) ("development" . ?v) ("security" . ?S) ("recruiting" . ?R)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("development" . ?v)
   (:grouptags . nil)
   ("daedalus" . ?D) ("icarus" . ?I) ("olympus" . ?O) ("vulcan" . ?V)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("ngc" . ?N)
   (:grouptags . nil)
   ("aac" . ?C) ("gws" . ?G)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("business" . ?b)
   (:grouptags . nil)
   ("author" . ?A) ("health-info" . ?H)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("personal" . ?p)
   (:grouptags . nil)
   ("family" . ?f) ("health" . ?h)
   (:endgrouptag . nil)
   (:startgrouptag . nil) ("family" . ?f)
   (:grouptags . nil)
   ("kmc" . ?k) ("sec" . ?s) ("eac" . ?e) ("extended" . ?x)
   (:endgrouptag . nil)
   (:startgrouptag . nil)
   ("health" . ?h)
   (:grouptags . nil)
   ("emotional" . ?E) ("mental" . ?M) ("physical" . ?P)
   (:endgrouptag . nil)
   ("appt" . ?a) ("meeting" . ?m))
 org-adapt-indentation t)

(cc/ensure-directory-exists org-archive-directory)

(require 'org)

(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes org-mode-map
    "," '("major mode" keymap)

    ",;" '("toggle" keymap)
    ",h" '("headings" keymap)
    ",o" '("outline" keymap)
    ",p" '("properties" keymap)
    ",r" '("roam" keymap)
    ",s" '("subtrees" keymap)

    (kbd ", <tab>") '("next link" . org-next-link)
    (kbd ", <backtab>") '("previous link" . org-previous-link)
    ",," '("C-c C-c" . org-ctrl-c-ctrl-c)
    ",c" '("capture" . org-capture)
    ",k" '("abort" . org-capture-kill)
    ",t" '("todo" . org-todo)

    ",;c" '("columns view" . org-columns)

    ",hb" '("backward" . org-backward-heading-same-level)
    ",hf" '("forward" . org-forward-heading-same-level)
    ",hg" '("goto" . org-goto)
    ",hh" '("left" . org-do-promote)
    ",hj" '("next" . org-next-visible-heading)
    ",hk" '("previous" . org-previous-visible-heading)
    ",hl" '("right" . org-do-demote)
    
    ",oa" '("expose all" . outline-show-all)
    ",ob" '("expose branches" . outline-show-branches)
    ",oc" '("expose children" . outline-show-children)
    ",ok" '("up level" . outline-up-heading)
    
    ",s^" '("sort" . org-sort)
    ",sc" '("copy" . org-copy-subtree)
    ",sm" '("mark" . org-subtree-mark)
    ",sh" '("left" . org-promote-subtree)
    ",sl" '("right" . org-demote-subtree)
    ",sj" '("down" . org-move-subtree-down)
    ",sk" '("up" . org-move-subtree-up)
    ",sp" '("paste" . org-paste-subtree)
    ",sr" '("refile" . org-refile)
    ",sx" '("cut" . org-cut-subtree)
    ",sy" '("yank" . org-yank)
    
    ",p." '("compute at point" . org-compute-property-at-point)
    ",p/" '("search" . org-match-sparse-tree)
    ",pa" '("action" . org-property-action)
    ",pd" '("delete" . org-delete-property)
    ",pD" '("delete globally" . org-delete-property-globally)
    ",ph" '("previous" . org-property-previous-allowed-value)
    ",pl" '("next" . org-property-next-allowed-value)
    ",pp" '("complete" . pcomplete-completions-at-point)
    ",ps" '("set" . org-set-property)))

(nconc org-modules '(org-id org-habit))

(with-eval-after-load 'org-habit
  (setq org-habit-preceding-days 14)
  (setq org-habit-following-days 14))

(require 'org-contrib)

;; TODO: Replace with org-superstar
(setq
 org-bullets-bullet-list
 '("◉" "○" "✸" "✿" "●" "◇" "✚" "✜" "☯" "◆" "♠" "♣" "♦" "☢" "❀" "◆" "◖" "▶"))

(require 'org-bullets)

(add-hook 'org-mode-hook #'org-bullets-mode)

;; TODO: Must find/package org-projectile for guix
;(setq
; org-confirm-elisp-link-function nil
; org-projectile-per-project-file-path "notes.org")
;(require 'org-projectile)
;(with-eval-after-load 'projectile
;  (with-eval-after-load 'cc-keys
;    (evil-define-key normalish-modes 'global
;      (concat normalish-prefix "o") '("todo" . org-projectile-project-todo-entry)
;      (concat normalish-prefix "r") '("todo read" . org-projectile-project-todo-completing-read))))
;(push (org-projectile-project-todo-entry) org-capture-templates)
;(setq org-agenda-files (append org-agenda-files (org-projectile-todo-files)))
  
(setq
 org-roam-directory (expand-file-name "~/notes")
 org-roam-capture-templates
 '(("d" "default" plain "%?"
    :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org.gpg" "#+title: ${title}\n")
    :unnarrowed t)))

(require 'org-roam)

(cc/enable org-roam-db-autosync-mode)

(evil-define-key normalish-modes 'global
  (concat normalish-prefix "nr") '("roam" keymap)
  (concat normalish-prefix "nrc") '("create node" . org-roam-capture)
  (concat normalish-prefix "nrf") '("find node" . org-roam-node-find)
  (concat normalish-prefix "nri") '("insert link" . org-roam-node-insert))

(evil-define-key normalish-modes org-roam-mode-map
  ",nrc" '("create node" . org-roam-capture)
  ",nrf" '("find node" . org-roam-node-find)
  ",nri" '("insert link" . org-roam-node-insert))

;; TODO: org-pomodoro
;; TODO: org-git
;; TODO: org-web-tools
;; TODO: org-vcard
;; TODO: org-sidebar
;; TODO: org-rich-yank
;; TODO: org-reveal (or maybe org-re-reveal)
;; TODO: org-ref
;; TODO: org-ql
;; TODO: org-pretty-table
;; TODO: org-noter (?: only if it provides some use over org-roam)
;; TODO: org-mime
;; TODO: org-jira
;; TODO: org-emms
;; TODO: org-edna
;; TODO: org-download
;; TODO: org-cv
;; TODO: org-contacts
;; TODO: org-beautify-theme
;; TODO: org-appear

(provide 'cc-org)
