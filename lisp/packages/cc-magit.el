(require 'cc-vars)

(setq
  magit-repository-directories '(("~/git/" . 2))
  magit-define-global-key-bindings nil)

(require 'magit)

(with-eval-after-load 'cc-keys
  (evil-define-key normalish-modes 'global
    (concat normalish-prefix "g")  '("git" keymap)
    (concat normalish-prefix "gb") '("git blame" . magit-blame)
    (concat normalish-prefix "gc") '("git clone" . magit-clone)
    (concat normalish-prefix "gi") '("git init" . magit-init)
    (concat normalish-prefix "gs") '("git status" . magit-status)))

(push 'trash magit-no-confirm)

(provide 'cc-magit)
