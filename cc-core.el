(require 'cc-utils)

(defvar cc/pre-init-hooks nil
  "A list of functions to execute before initialization.")

(defvar cc/post-init-hooks nil
  "A list of functions to execute after initialization.")

(defun cc/initialize-modes ()
  "Enable/Disable modes."
  (cc/enable
   column-number-mode)

  (cc/disable
   global-hl-line-mode
   global-undo-tree-mode
   menu-bar-mode
   scroll-bar-mode
   tool-bar-mode
   tooltip-mode))

(defun cc/initialize-variables ()
  "Initialize Emacs variable defaults."
  (setq-default
   indent-tabs-mode nil
   inhibit-startup-message t
   backup-inhibited t
   epg-pinentry-mode 'loopback
   custom-file (expand-file-name ".custom.el" user-emacs-directory)
   initial-scratch-message nil)

  (fset 'yes-or-no-p #'y-or-n-p)

  (setq
   user-full-name "Cameron V Chaparro"
   user-mail-address "cameron@cameronchaparro.com"))

(defun cc/setup-hooks ()
  "Set up all default hooks."
  (add-hook 'cc/post-init-hooks #'pinentry-start)
  (add-hook 'cc/post-init-hooks #'server-start)
  (add-hook 'cc/post-init-hooks (lambda () (cc/maybe-load-file custom-file)))
  (mapc (lambda (hook) (add-hook hook 'linum-mode)) '(prog-mode-hook)))

(defun cc/initialize ()
  "Initialize the configuration."
  (mapc (lambda (fn) (funcall fn)) cc/pre-init-hooks)
  (cc/initialize-modes)
  (cc/initialize-variables)
  (cc/setup-hooks)
  (mapc (lambda (fn) (funcall fn)) cc/post-init-hooks))

(provide 'cc-core)
